# Full-stack Take Home Project

## Prerequisites

You may need to install the following to get started

- [Docker](https://www.docker.com/products/docker-desktop)
- [Docker Compose](https://docs.docker.com/compose/install/)
- [Node.js 16.14.0 LTS](https://nodejs.org/dist/v16.14.0/)

## Goal

Migrate the project from REST to GraphQL

### Requirements

1. Backend: Implement the REST endpoint (`rest.js`) business logic in the GraphQL endpoint (`graphql.js`)

2. Frontend: [Add Apollo Client and use their React hooks](https://www.apollographql.com/docs/react/get-started/) to query the GraphQL endpoint for applications, instead of REST. The frontend should still display the loading and error states, and the tests should still pass.

You can use any dependencies you like, and feel free to reach out to your buddy to clarify requirements.

When you have finished the project, please create a PR and add comments to explain your design.

### Bonus

If you have time:

- add more tests
- in the UI below each application, display the application features

## Get Started

### Backend

#### Install dependencies

```
cd backend
npm i
```

#### Start the local DynamoDB

```
docker compose up dynamo-local
# OR
docker-compose up dynamo-local
```

You may access the admin page by http://localhost:8001

#### Create and Seed tables

```
node createTables.js
node seedTables.js
```

There will be two tables created, `applications` and `application_features`.

The data relationship is as below

![UML](./images/uml.png)

#### Run the backend REST endpoint

```
node rest.js
```

#### Run the backend GRAPHQL endpoint

```
node graphql.js
```

### Frontend

#### Install dependencies

```
cd frontend
npm i
```

### Run the frontend

```
npm start
```

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### Run the frontend tests

```
npm test
```

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

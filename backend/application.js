const _ = require('lodash');
const restify = require('restify');
const util = require('util');
const AWS = require('./aws');

AWS.config.update({
  region: 'us-west-2',
  endpoint: 'http://localhost:8000',
});

function InvalidApplicationError(msg) {
  restify.RestError.call(this, {
    statusCode: 400,
    restCode: 'InvalidApplicationError',
    message: `Params did not pass validation: ${msg}`,
    constructorOpt: InvalidApplicationError,
  });

  this.name = 'InvalidApplicationError';
}
util.inherits(InvalidApplicationError, restify.RestError);

function listApplicationsByOrgId(orgId) {
  const docClient = new AWS.DynamoDB.DocumentClient();
  return docClient
    .query({
      TableName: 'applications',
      KeyConditionExpression: 'organization = :orgId',
      ExpressionAttributeValues: {
        ':orgId': orgId,
      },
    })
    .promise()
    .then((data) => data.Items);
}

function getApplications(req, res, next) {
  const { org_id } = req.params;

  return listApplicationsByOrgId(org_id)
    .then((app_list) => {
      res.send(200, app_list);
      return next();
    })
    .catch(next);
}

module.exports = {
  getApplications,
};

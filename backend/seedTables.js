const fs = require('fs');
const AWS = require('./aws');

const docClient = new AWS.DynamoDB.DocumentClient();

const seed = (tableName, pathToJSONSeed) => {
  const itemsToSeed = JSON.parse(fs.readFileSync(pathToJSONSeed, 'utf8'));
  itemsToSeed.forEach((item) => {
      const params = {
          TableName: tableName,
          Item: item
      };
  
      docClient.put(params, (err) => {
          const { uuid } = item;
          if (err) {
              console.error(`Unable to add item to ${tableName}:`, uuid, ". Error JSON:", JSON.stringify(err, null, 2));
          } else {
              console.log(`PutItem in ${tableName} succeeded:`, uuid);
          }
      });
  });
}

// Seed Data
console.log("Importing data into DynamoDB. Please wait... \n");
seed('applications', 'applications.json');
seed('application_features', 'application_features.json');

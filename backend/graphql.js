const { ApolloServer } = require('apollo-server');
const { GraphQLGUID } = require('graphql-scalars');

const typeDefs = `
scalar GUID

type Feature {
  uuid: GUID!
  key: String!
  enabled: Boolean!
}

type Application {
  organization: GUID!
  uuid: GUID!
  awsmaApplicationId: GUID
  contentRating: String!
  productVersion: String!
  useDefaultPushCategories: Boolean!
  features: [Feature!]!
}

type Organization {
  applications: [Application!]!
}

type Query {
  organization(uuid: GUID!): Organization 
}

input CreateApplicationInput {
  organization: GUID!
  contentRating: String!
  productVersion: String!
  useDefaultPushCategories: Boolean!
  enableAppEngagementAnalytics: Boolean = false
}

type Mutation {
  createApplication(input: CreateApplicationInput!): Application!
}
`

const server = new ApolloServer({
  typeDefs,
  mocks: true,
  resolvers: {
    GUID: GraphQLGUID,
  },
});

server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
})

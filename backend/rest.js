const restify = require('restify');
const Application = require('./application');

function run() {
  const server = restify.createServer();
  server.use(restify.queryParser());
  server.use(restify.jsonBodyParser({ mapParams: false }));

  server.get('/organizations/:org_id/applications', Application.getApplications);

  server.listen(8080, () => {
    console.log('%s listening at %s', server.name, server.url);
  });
}

run();

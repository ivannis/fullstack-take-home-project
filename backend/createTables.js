const Promise = require('bluebird');
const AWS = require('./aws');

const TABLE_DEFINITIONS = [
  {
    TableName: 'applications',
    KeySchema: [
      { AttributeName: 'organization', KeyType: 'HASH' },
      { AttributeName: 'uuid', KeyType: 'RANGE' },
    ],
    AttributeDefinitions: [
      { AttributeName: 'organization', AttributeType: 'S' },
      { AttributeName: 'uuid', AttributeType: 'S' },
    ],
    ProvisionedThroughput: {
      ReadCapacityUnits: 1,
      WriteCapacityUnits: 1,
    },
  },
  {
    TableName: 'application_features',
    KeySchema: [
      { AttributeName: 'application', KeyType: 'HASH' },
      { AttributeName: 'uuid', KeyType: 'RANGE' },
    ],
    AttributeDefinitions: [
      { AttributeName: 'application', AttributeType: 'S' },
      { AttributeName: 'uuid', AttributeType: 'S' },
    ],
    ProvisionedThroughput: {
      ReadCapacityUnits: 1,
      WriteCapacityUnits: 1,
    },
  },
];

function createTables() {
  const dynamodb = new AWS.DynamoDB();
  return Promise.map(
    TABLE_DEFINITIONS,
    (tableDefinition) => dynamodb.createTable(tableDefinition).promise(),
  )
    .then(() => console.log('Tables have been created'))
    .catch((err) => console.error(err));
}

createTables();

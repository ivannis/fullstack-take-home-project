import { render, screen } from '@testing-library/react';
import { rest } from 'msw'
import { setupServer } from 'msw/node'
import App from './App';

const server = setupServer(
  rest.get('/organizations/:org_id/applications', (req, res, ctx) => {
    return res(ctx.json(
      [{
          content_rating: "4+",
          name: "The Best App in the World",
          organization: "6c942a50-5524-4df8-bead-2389d02e24e2",
          product_version: "EA4",
          use_default_push_categories: true,
          uuid: "0e24ed9b-dafc-416f-b90c-b8e6715a20b4"
      }]
    ))
  }),
)

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

test('renders applications', async () => {
  render(<App />);
  expect(screen.getByText("Loading applications...")).toBeInTheDocument();
  await screen.findByText("The Best App in the World")
  expect(screen.getByText("Organization Applications")).toBeInTheDocument();
  expect(screen.getByText("Content rating: 4+")).toBeInTheDocument();
  expect(screen.getByText("The Best App in the World")).toBeInTheDocument();
  expect(screen.queryByText("Oops! failed to fetch applications.")).not.toBeInTheDocument();
});

test('handle fetch errors', async () => {
  // TODO - test case for fetch error
});

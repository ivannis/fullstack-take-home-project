import React from "react";

import "./App.css";

const ORG_ID = '6c942a50-5524-4df8-bead-2389d02e24e2';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      applications: [],
      hasError: false,
      loading: true,
    };
  }

  searchByOrg(orgId) { 
    fetch(`/organizations/${orgId}/applications`)
        .then(res => { 
          if (res.ok) {
            return res.json()
          } else {
            throw new Error('Something went wrong')
          }
        })
        .then(data => this.setState({ applications: data, loading: false }))
        .catch(data => this.setState({ hasError: true, loading: false }));
  }

  componentDidMount() {
    this.searchByOrg(ORG_ID)
  }

  render() {
    const  { applications, loading, hasError } = this.state;

    if (loading) { 
      return <h3>Loading applications...</h3>;
    }

    if (hasError) { 
      return <h3 className="app__msg">Oops! failed to fetch applications.</h3>;
    }

    return (
      <div className="app">
        <h1>Organization Applications</h1>
        {
          applications.length > 0 ? 
            (
              <div className="apps__container" >
                {applications.map(({ uuid, content_rating, name }) => (
                  <div className="app__wrapper" key={uuid}>
                    {name && <h2 className="app__title">{name}</h2>}
                    {content_rating && <p className="app__details">Content rating: {content_rating}</p>}
                  </div>
                ))}
              </div>
            ) : <h3 className="app__msg">No applications found.</h3>
        }
      </div>
    );
  }
}

export default App;
